# Installation NEXUS and push local docker image

## STEP:1 create or open daemon.json

```bash
sudo vi /etc/docker/daemon.json
```

## STEP:2 - add insecure registry
```bash
{
  "insecure-registries" : ["localhost:8089"]
}
```


## STEP:3 - run nexus and nginx

```bash
{
  docker-compose up
}
```

## STEP:4 - log in docker registry

```bash
{
  docker login -u admin http://localhost:8089
}
```
## STEP:5 - add tag to our nginx image

```bash
{
  docker tag custom-nginx localhost:8089/custom/nginx:v1
}
```

## STEP:6 - push image

```bash
{
  docker push localhost:8089/custom/nginx:v1
}
```




